# Pretty Merge Requests for Gitlab

**Disclaimer**: This repo is heavily based on [git-pretty-pull-requests](https://github.com/williamdclt/git-pretty-pull-request) by [williamdclt](https://github.com/williamdclt/git-pretty-pull-request/commits?author=williamdclt) which serves the same purpose for Github.

## Overview

This is a bash script for opening Gitlab Merge requests from your command line.

It allows to open multiple MRs at the same times (staging/preprod/prod for example) and to check the commits in each of these MRs, and handles MR templates.

It uses the `lab` CLI tool which is available [here](https://github.com/zaquestion/lab).

## Install

- Make sure you've installed [Lab](https://github.com/zaquestion/lab).
- **Run `lab browse` once for Lab to set your credentials**.
- Download the git-pretty-merge-request script in this repository and make sure it's available in your $PATH (put it in /usr/local/bin for example).
  - Symlinking the file is the easiest way of doing it, to do so you can use the following command:
  - `ln -s <PATH/TO/REPO>/git-pretty-merge-request/git-pretty-merge-request /usr/local/bin`
- Make sure the script is executable : `chmod +x /usr/local/bin/git-pretty-merge-request`
- In each of your projects, set the branches on which you want to open merge requests: `git config pretty-merge-request.merge-bases "staging preprod prod"` (or set it globally with the `--global` option)
- (optional) `git pretty-merge-request` is tedious to type, I'd advise to alias it: `git config --global alias.mr pretty-merge-request`.

`git mr` is now an alias to `git pretty-merge-request`, which will call your git-pretty-merge-request script if it can be found in your $PATH!

## Labels configuration

You can add additional configurations in your projects. These will add labels to your merge requests automatically.

### Environment label

git-pretty-merge-request can be configured to add an `ENV::<ENV_NAME>` label to your merge rquests.
To do so you must configure which branches correspond to which environment. git-pretty-merge-request handles 3 environments:

- PRODUCTION
- PRE-PRODUCTION
- STAGING

If you use the following configuration: `git config pretty-merge-request.merge-bases "staging preprod prod"` you can tell git-pretty-merge-request which branches correspond to which environments by running the following commands:

```bash
git config pretty-merge-request.production-branch "prod"
git config pretty-merge-request.pre-production-branch "preprod"
git config pretty-merge-request.staging-branch  "staging"
```

This will add the tag `ENV::PRODUCTION` (resp. `ENV::PRE-PRODUCTION` AND `ENV::STAGING`) to all your merge requests on the `prod` (resp. `preprod` and `staging`) branch.

The source branch will only be deleted when merged into the production branch.

### Team labels

If several teams work on the project, it can be useful to tag each merge request with each team.
To set our team label automatically you can use the following command:

```bash
git config pretty-merge-request.team-label "Team::ExampleTeam"
```

This will add the tag `Team::ExampleTeam` to all your merge requests.

## Opening Merge Requests

Instead of going on Gitlab and manually opening MRs, run `git mr` and tada! Check that the MR that will be opened contain what you expect, add an assignee and an extra tag if you want to and confirm: that's it.

If your project has a merge request template, your editor will be prompted to edit it.

## Contributing

All contributions, merge requests, issues or feature requests are very welcome!
